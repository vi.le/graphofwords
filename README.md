<!--
SPDX-FileCopyrightText: 2019 Vincent Lequertier <vi.le@autistici.org>
SPDX-License-Identifier: GPL-3.0-only
-->

# Graph of words

[![REUSE status](https://api.reuse.software/badge/gitlab.com/vi.le/graphofwords)](https://api.reuse.software/info/gitlab.com/vi.le/graphofwords)
[![PyPI version](https://img.shields.io/pypi/v/graph-of-words.svg)](https://pypi.python.org/pypi/graph-of-words)

Create a graph of word representation from a text. Roughly based on *Malliaros, F. D., & Skianis, K. (2015). Graph-Based Term Weighting for Text Categorization. 1473–1479. https://doi.org/10.1145/2808797.2808872*

Example:

```python
from graph_of_words import GraphOfWords

graph = GraphOfWords(window_size=2)
graph.build_graph(
    'Roses are red. Violets are blue',
    # OR a sentences list['Roses  are  red.', 'Violets are blue'],
    remove_stopwords=False,
    workers=4
)

graph.display_graph()
graph.write_graph_edges('edges_list.txt')
```

Note that `build_graph` also accepts a list of sentences.

The following actions are performed:

1. Split the text into sentences if required
2. Slide a window across each sentence
3. Add a directed graph edge form the current word to the other ones within the
   window, the weight being the distance between the two words

## Documentation

See [the documentation](http://vi.le.gitlab.io/graphofwords/).
