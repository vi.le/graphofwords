.. SPDX-License-Identifier: GPL-3.0-only
.. SPDX-FileCopyrightText: 2020 Vincent Lequertier <vi.le@autistici.org>


Graph of Words
==============

Create a graph of word representation from a text. Roughly based on *Malliaros, F. D., & Skianis, K. (2015). Graph-Based Term Weighting for Text Categorization. 1473–1479. https://doi.org/10.1145/2808797.2808872*

.. code-block:: python

   from graph_of_words import GraphOfWords

   graph = GraphOfWords(window_size=2)
   graph.build_graph(
       'Roses are red. Violets are blue',
       # OR a sentences list['Roses  are  red.', 'Violets are blue'],
       remove_stopwords=False,
       workers=4
   )

   graph.display_graph()
   graph.write_graph_edges('edges_list.txt')

.. automodule:: graph_of_words
   :members:
   :undoc-members:
